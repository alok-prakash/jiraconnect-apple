//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension UIColor {
  convenience init(_ hex6: UInt32, alpha: CGFloat = 1) {
    let divisor = CGFloat(255)
    let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
    let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
    let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
    self.init(red: red, green: green, blue: blue, alpha: 1)
  }
}

// Instead do UIColor.ADGRedColor()

// Primary
let ADGNavy = UIColor(0x205081)
let ADGBlue = UIColor(0x3572B0)
let ADGPaleBlue = UIColor(0xEBF2F9)
let ADGGreen = UIColor(0x14892C)
let ADGYellow = UIColor(0xF6C342)
let ADGRed = UIColor(0xD04437)
let ADGCharcoal = UIColor(0x333333)
let ADGMediumGray = UIColor(0x707070)
let ADGLightGray1 = UIColor(0xF5F5F5)
let ADGLightGray2 = UIColor(0xF7F7F7)
let ADGLightGray3 = UIColor(0xCDCFD5)

let ADGBrighterBlue = UIColor(0x296CB1)

// Secondary
let ADGGray = UIColor(0x999999)

let ADGAshGray1 = UIColor(0xCCCCCC)
let ADGAshGray2 = UIColor(0xE2E2E2)

let ADGSilver = UIColor(0xE9E9E9)
let ADGBrown = UIColor(0x815B3A)
let ADGCheetoOrange = UIColor(0xF79232)
let ADGTan = UIColor(0xF1A257)
let ADGLightBrown = UIColor(0xD39C3F)
let ADGCyan = UIColor(0x59AFE1)

// Derivative
let ADGSuperBrightBlue = UIColor(0x1A8CFF)


let ADGNavigationBarColor = ADGBrighterBlue
let ADGNavigationBarTintColor = UIColor.whiteColor()
let ADGTintColor = ADGBrighterBlue
let ADGChromeColor = ADGLightGray2
let ADGHairlineColor = ADGAshGray2
let ADGTextColor = ADGCharcoal
let ADGTextColorSecondary = ADGLightGray3
