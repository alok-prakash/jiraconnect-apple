//
//  JIRAMobileConnectForPlayground.h
//  JIRAMobileConnectForPlayground
//
//  Created by Rene Cacheaux on 12/4/15.
//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JIRAMobileConnectForPlayground.
FOUNDATION_EXPORT double JIRAMobileConnectForPlaygroundVersionNumber;

//! Project version string for JIRAMobileConnectForPlayground.
FOUNDATION_EXPORT const unsigned char JIRAMobileConnectForPlaygroundVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JIRAMobileConnectForPlayground/PublicHeader.h>


