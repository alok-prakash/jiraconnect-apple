//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import XCTest
import JIRAMobileConnect

class JIRAMobileConnectUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
        XCUIDevice.sharedDevice().orientation = .Portrait
    }
    
    func testNewFeedback() {
        let app = XCUIApplication()
        app.windows["JMCExampleAppWindow"].tap()
        let entryPromptSheet = app.sheets["Feedback Box"]
        XCTAssertTrue(entryPromptSheet.exists)
        XCTAssertTrue(entryPromptSheet.collectionViews.buttons["New Feedback"].exists)
        entryPromptSheet.collectionViews.buttons["New Feedback"].tap()
        
        let navBar = app.navigationBars["New Feedback"]
        let exists = NSPredicate(format: "exists == 1")
        expectationForPredicate(exists, evaluatedWithObject: navBar, handler: nil)
        waitForExpectationsWithTimeout(2, handler: nil)
        
        let cancelButton = XCUIApplication().navigationBars["New Feedback"].buttons["Cancel"]
        XCTAssertTrue(cancelButton.exists)
        cancelButton.tap()
    }
}
